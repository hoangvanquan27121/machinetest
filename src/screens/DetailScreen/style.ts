import {StyleSheet} from 'react-native';
import {Colors} from '../../themes';
import {HEIGHT_SCREEN} from '../../constants/Screen';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  body: {
    flex: 1,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  image: {
    width: '100%',
    height: HEIGHT_SCREEN / 3,
  },
  itemTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.black,
  },
  itemContent: {
    fontSize: 14,
    color: Colors.gray,
    marginTop: 10,
  },
});
