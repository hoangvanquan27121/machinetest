import React from 'react';
import { Image, ScrollView, Text, View } from "react-native";
import styles from './style';
import Header from '../../components/Header';
import {IMG_NOTE} from '../../assets';
import {INote} from '../../models';
import {useRoute} from '@react-navigation/native';

interface Props {
  navigation: any;
}
const DetailScreen = (props: Props) => {
  const {navigation} = props;

  //Router
  const route = useRoute<any>();
  const data: INote = route.params.data;

  // Function
  const _goBack = () => {
    navigation.goBack();
  };
  // Render IU
  return (
    <View style={styles.container}>
      <Header isShowBackButton={true} title="Detail Screen" goBack={_goBack} />
      <Image style={styles.image} source={IMG_NOTE} />
      <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
        <Text style={styles.itemTitle}>{data.title}</Text>
        <Text style={styles.itemContent}>{data.content}</Text>
      </ScrollView>
    </View>
  );
};
export default DetailScreen;
