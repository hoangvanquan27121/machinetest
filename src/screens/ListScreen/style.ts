import {StyleSheet} from 'react-native';
import {Colors} from '../../themes';
import colors from '../../themes/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  wrapNotes: {
    flex: 1,
    marginTop: 10,
  },
  textInput: {
    fontSize: 16,
    borderWidth: 1,
    marginHorizontal: 8,
    marginTop: 8,
    borderRadius: 10,
    padding: 8,
    borderColor: Colors.gray,
  },
  wrapItem: {
    borderWidth: 1,
    borderColor: Colors.gray,
    padding: 10,
    borderRadius: 10,
    marginBottom: 8,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  itemTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.black,
  },
  itemContent: {
    fontSize: 14,
    color: Colors.gray,
  },
  itemImage: {
    width: 70,
    height: 70,
    resizeMode: 'cover',
    marginRight: 8,
  },
  wrapItemRight: {
    flex: 1,
  },
});
