import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import styles from './style';
import Header from '../../components/Header';
import {INote} from '../../models';
import routerName from '../../router/routerName';
import LoaderIndicator from '../../components/LoaderIndicator';
import {notes} from '../../mocks';
import {Colors} from '../../themes';
import {IMG_NOTE} from '../../assets';

interface Props {
  navigation: any;
}
const ListScreen = (props: Props) => {
  const {navigation} = props;
  const [search, setSearch] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState<INote[]>([]);
  const [timeOutSearch, setTimeOutSearch] = useState<NodeJS.Timeout>();

  // Life Circle
  useEffect(() => {
    _initData();
  }, []);

  // Function
  const _initData = () => {
    setLoading(true);
    setTimeout(() => {
      setData(notes);
      setLoading(false);
    }, 3000);
  };
  const _onPressItem = (item: INote) => {
    navigation.navigate(routerName.DetailScreen, {data: item});
  };
  const _onChangeText = (value: string) => {
    setSearch(value);
    clearTimeout(timeOutSearch);
    const timeOut = setTimeout(() => {
      if (value === '') {
        setData(notes);
        return;
      }
      const newList = notes.filter(
        e => e.title.includes(value) || e.content.includes(value),
      );
      setData(newList);
    }, 1000);
    setTimeOutSearch(timeOut);
  };

  const _onRefresh = () => {
    setSearch('');
    _initData();
  };

  // Render IU
  const _renderItem = ({item}: {item: INote}) => (
    <TouchableOpacity
      style={styles.wrapItem}
      onPress={() => _onPressItem(item)}
      key={item.id}>
      <Image style={styles.itemImage} source={IMG_NOTE} />
      <View style={styles.wrapItemRight}>
        <Text style={styles.itemTitle}>{item.title}</Text>
        <Text style={styles.itemContent}>
          {item.content.substring(0, 200)}...
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <Header title="List Screen" />
      <TextInput
        value={search}
        style={styles.textInput}
        placeholder="Search"
        placeholderTextColor={Colors.gray}
        onChangeText={_onChangeText}
      />
      <FlatList
        style={styles.wrapNotes}
        data={data}
        renderItem={_renderItem}
        onRefresh={_onRefresh}
        refreshing={false}
      />
      <LoaderIndicator loading={isLoading} />
    </View>
  );
};
export default ListScreen;
