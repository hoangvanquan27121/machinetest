import {CreateAxiosDefaults} from 'axios';

const axiosConfig: CreateAxiosDefaults = {
  baseURL: 'https://xxxxx.xxx',
  responseType: 'json',
  timeout: 30000,
};

export {axiosConfig};
