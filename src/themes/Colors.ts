const Colors = {
  transparent: 'transparent',
  blue: '#707070',
  white: '#ffffff',
  black: '#000000',
  gray: 'gray',
};

export default Colors;
