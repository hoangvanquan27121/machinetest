import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');
const SMALL_SCREEN = width < 375 || height < 667;
const BIG_SCREEN = width > 375 || height > 667;
const STANDARD_SCREEN = width === 375 || height === 667;

const WIDTH_SCREEN = width;
const HEIGHT_SCREEN = height;

export {SMALL_SCREEN, BIG_SCREEN, STANDARD_SCREEN, WIDTH_SCREEN, HEIGHT_SCREEN};
