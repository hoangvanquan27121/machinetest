import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import styles from './LoaderIndicator.style';

type Props = {
  loading: boolean;
};

const LoaderIndicator = (props: Props) => {
  const {loading} = props;

  const _renderLoader = () => (
    <View style={styles.container}>
      <View style={[styles.modalBackground]}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
      </View>
    </View>
  );

  return loading ? _renderLoader() : null;
};

export default LoaderIndicator;
