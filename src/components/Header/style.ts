import {StyleSheet} from 'react-native';
import {Colors} from '../../themes';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: 50,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: Colors.gray,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 50,
  },
  title: {
    color: Colors.black,
    fontSize: 20,
  },
  right: {
    width: 50,
  },
});
