import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './style';

interface Props {
  title: string;
  isShowBackButton?: boolean;
  goBack?: () => void;
}
const Header = (props: Props) => {
  const {title, isShowBackButton, goBack} = props;

  const _renderBackButton = () => {
    return (
      <TouchableOpacity onPress={goBack}>
        <Text>{'< Back'}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      {isShowBackButton && _renderBackButton()}
      {!isShowBackButton && <View style={styles.right} />}
      <Text style={styles.title}>{title}</Text>
      <View style={styles.right} />
    </View>
  );
};
export default Header;
