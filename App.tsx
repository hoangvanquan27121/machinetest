import React from 'react';
import AppNavigation from './src/router';
import {Provider} from 'react-redux';
import {store, persist} from './src/redux/store/configureStore';
import {PersistGate} from 'redux-persist/integration/react';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persist}>
        <AppNavigation />
      </PersistGate>
    </Provider>
  );
};

export default App;
